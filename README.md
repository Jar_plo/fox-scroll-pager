# `fox-scroll-pager` 组件文档
`fox-scroll-pager` 组件实现了一个可水平滚动的分页器，适用于需要分页显示内容的场景，如图片画廊、商品展示等。该组件支持动态计算页面的总数，并提供指示器来显示当前页码。


**`fox-scroll-pager`** 是一款轻量级、灵活的水平滚动分页组件，专为需要展示可滑动内容的场景设计。无论是图片画廊、商品展示还是任何需要分页显示的内容，`fox-scroll-pager` 都能完美应对。

### 组件亮点：
- **智能分页**：自动根据内容宽度计算总页数，并根据滚动位置动态更新当前页。
- **可定制化指示器**：支持显示精美的分页指示器，可自定义指示器颜色、大小和位置，完美契合各种设计需求。
- **平滑的滚动体验**：滑动操作流畅，触摸结束后会自动定位到最接近的页面，确保用户体验的流畅和自然。
- **响应式支持**：组件能够根据容器宽度自适应调整，兼容多种设备尺寸。

### 适用场景：
- 商品列表展示
- 图片画廊/轮播图
- 任意需要分页展示的大型内容列表

如果你在开发中需要一个简洁、灵活的分页滚动组件，`fox-scroll-pager` 将是你理想的选择！它不仅简单易用，还能轻松适配各种页面布局，让你的项目更加流畅、美观。

赶紧使用 `fox-scroll-pager` 组件来提升你的页面体验吧！

## 功能特性

- **支持水平滚动**：组件通过水平滚动展示内容，自动计算总页数。
- **指示器显示**：可以显示当前页面的指示器，支持自定义颜色和位置。
- **滚动位置控制**：支持通过事件更新当前滚动位置和页码。
- **触摸结束定位**：通过触摸结束事件，自动将滚动位置调整到最近的页面。

---

## 组件属性

| 属性名               | 类型      | 默认值       | 说明                                                     |
|----------------------|-----------|--------------|----------------------------------------------------------|
| `initialScrollLeft`   | `Number`  | `0`          | 初始化时的滚动位置，单位为像素。                         |
| `indicator`           | `Boolean` | `true`       | 控制是否显示面板指示器。                                   |
| `indicatorColor`      | `String`  | `#999`       | 指示器非激活状态的颜色。                                   |
| `indicatorActiveColor`| `String`  | `#2AA4F4`    | 指示器激活状态的颜色。                                     |
| `indicatorPosition`   | `String`  | `10px`       | 指示器距离底部的距离，单位为像素。                         |

---

## 组件数据

| 数据名                | 类型      | 说明                                                     |
|-----------------------|-----------|----------------------------------------------------------|
| `scrollLeftVal`       | `Number`  | 当前的滚动位置，单位为像素。                             |
| `currentPageIndexVal` | `Number`  | 当前页的索引，从 `0` 开始计数。                           |
| `lastScrollLeft`      | `Number`  | 上一次的滚动位置，用于判断滑动方向。                     |
| `contentWidthVal`     | `Number`  | 内容总宽度，单位为像素。                                 |
| `totalPages`          | `Number`  | 总页数。                                                 |
| `temporarilyLeftVal`  | `Number`  | 临时存储的滚动位置，用于在触摸结束时计算当前页。         |
| `showoverlay`         | `Boolean` | 是否显示滚动结束的遮罩层。                               |

---

## 事件

| 事件名                | 参数                | 说明                                                       |
|-----------------------|---------------------|------------------------------------------------------------|
| `update:scrollLeft`   | `scrollLeft`        | 当滚动位置变化时触发，更新滚动位置。                       |
| `update:currentPageIndex` | `currentPageIndex` | 当页面索引变化时触发，更新当前页码。                      |

---

## 方法

#### `calculatePageIndex(scrollLeft, offsetWidth)`
根据当前滚动位置和偏移宽度，计算当前的页码索引。

**参数**：
- `scrollLeft`: 当前滚动位置，单位为像素。
- `offsetWidth`: 单个页面的宽度，单位为像素。

**返回值**：
- 返回当前页面的索引。

#### `getWindow()`
获取滚动容器的位置信息，并返回一个 Promise。

**返回值**：
- 返回一个 Promise，成功时解析为容器位置信息对象，失败时返回 `null`。

#### `getWinWidth()`
获取滚动容器的宽度，返回 Promise。

**返回值**：
- 返回容器的宽度，单位为像素。

#### `calculateTotalPages()`
根据内容宽度和滚动容器宽度，计算总页数。

#### `onScroll(e)`
滚动事件的处理函数，更新滚动位置并触发页码更新。

**参数**：
- `e.detail.scrollLeft`: 当前滚动位置。
- `e.detail.scrollWidth`: 内容总宽度。

#### `gridlistTouchend(e)`
触摸结束时，自动将滚动位置调整到最近的页面位置，并更新当前页码。

---

## 使用说明

#### 基本用法

```html
<template>
	<view>
		<fox-scroll-pager>
			<!-- 参考1 -->
			<!-- <view style="display: flex;">
				<view class="swiper" v-for="(item,index) in SwiperList" :key="index">
					<view class="wrapper" v-for="(citm,cidx) in item" :key="cidx">
						<view class="image" :style="`--bgcolor:${citm.bgColor};--brcolor:${citm.brColor}`">{{citm.image}}</view>
						<view class="text">{{citm.text}}</view>
					</view>
				</view>
			</view> -->
			<!-- 参考2 -->
			<view style="display: flex;width: 100%;flex-shrink: 0;flex-wrap: nowrap;">
				<view v-for="(item,index) in 7" :key="index" style="width:300px;background-color: antiquewhite;height: 300px;flex-shrink: 0;word-wrap: normal;">{{item}}</view>
			</view>
		</fox-scroll-pager>
	</view>
</template>

<script>
	export default {
		data() {
			return {
				title: 'Hello',
				SwiperList:[
					[
						{
							image:'美食',
							text:'美食',
							bgColor:'#F84819',
							brColor: '#F6412D',
						},
						{
							image:'卖场',
							text:'卖场便利',
							bgColor:'#2AA4F4',
							brColor: '#2AA4F4',
						},
						{
							image:'水果',
							text:'水果',
							bgColor:'#44C241',
							brColor: '#44C241',
						},
						{
							image:'跑腿',
							text:'跑腿代购',
							bgColor:'#2AA4F4',
							brColor: '#2AA4F4',
						},      
						{
							image:'甜品',
							text:'甜品饮品',
							bgColor:'#F488C0',
							brColor: '#F488C0',
						},
						{
							image:'星选',
							text:'星选好店',
							bgColor:'#F84819',
							brColor: '#F84819',
						},
						{
							image:'药店',
							text:'送药上门',
							bgColor:'#F84819',
							brColor: '#F84819',
						},
						{
							image:'大牌',
							text:'大牌惠吃',
							bgColor:'#F99104',
							brColor: '#F99104',
						},
						{
							image:'秒送',
							text:'取送件',
							bgColor:'#2AA4F4',
							brColor: '#2AA4F4',
						},
						{
							image:'签到',
							text:'签到赢饭钱',
							bgColor:'#FF9800',
							brColor: '#FF9800',
						},
					],
					[
						{
							image:'美食',
							text:'美食',
							bgColor:'#F84819',
							brColor: '#F6412D',
						},
						{
							image:'卖场',
							text:'卖场便利',
							bgColor:'#2AA4F4',
							brColor: '#2AA4F4',
						},
						{
							image:'水果',
							text:'水果',
							bgColor:'#44C241',
							brColor: '#44C241',
						},
						{
							image:'跑腿',
							text:'跑腿代购',
							bgColor:'#2AA4F4',
							brColor: '#2AA4F4',
						},      
						{
							image:'甜品',
							text:'甜品饮品',
							bgColor:'#F488C0',
							brColor: '#F488C0',
						},
						{
							image:'星选',
							text:'星选好店',
							bgColor:'#F84819',
							brColor: '#F84819',
						},
						{
							image:'药店',
							text:'送药上门',
							bgColor:'#F84819',
							brColor: '#F84819',
						},
						{
							image:'大牌',
							text:'大牌惠吃',
							bgColor:'#F99104',
							brColor: '#F99104',
						},
						{
							image:'秒送',
							text:'取送件',
							bgColor:'#2AA4F4',
							brColor: '#2AA4F4',
						},
						{
							image:'签到',
							text:'签到赢饭钱',
							bgColor:'#FF9800',
							brColor: '#FF9800',
						},
					]
				]
			}
		},
		onLoad() {

		},
		methods: {

		}
	}
</script>

<style scoped lang="scss">
	view{
		box-sizing: border-box;
	}
	.swiper{
	    display: grid;
	    padding: 0 20rpx;
	    grid-template-columns: repeat(5, minmax(0, 1fr));
	    gap: 5px;
	    width: 100%;
	    flex-shrink: 0;
	    .wrapper{
	        display: flex;
	        flex-direction: column;
	        align-items: center;
	        .image{
	            width: 90rpx;
	            height: 90rpx;
	            line-height: 34rpx;
	            background-color: var(--bgcolor);
	            border: 1px solid var(--brcolor);
	            color: rgba(255, 255, 255, 1);
	            font-size: 24rpx;
	            display: flex;
	            justify-content: center;
	            align-items: center;
	            border-radius: 100%;
	        }
	        .text{
	            height: 36rpx;
	            opacity: 0.68;
	            color: rgba(16, 16, 16, 1);
	            font-size: 24rpx;
	            margin-top: 10rpx;
	        }
	    }
	}
</style>

```

#### 高级用法
```html
<template>
  <fox-scroll-pager
    :initialScrollLeft="0"
    :indicator="true"
    :indicatorColor="'#ccc'"
    :indicatorActiveColor="'#42b983'"
    :indicatorPosition="'15px'"
    @update:scrollLeft="onScrollLeftUpdate"
    @update:currentPageIndex="onPageIndexUpdate"
  >
    <view class="content">
      <!-- 可以动态生成页面内容 -->
    </view>
  </fox-scroll-pager>
</template>

<script>
export default {
  data() {
    return {
      currentPageIndex: 0,
      scrollLeft: 0,
    };
  },
  methods: {
    onScrollLeftUpdate(scrollLeft) {
      this.scrollLeft = scrollLeft;
    },
    onPageIndexUpdate(pageIndex) {
      this.currentPageIndex = pageIndex;
    },
    reloadComponent() {
      // 使用重新加载组件的方式更新页面
      this.$refs.foxScrollPager.scrollLeftVal = 0;
    },
  },
};
</script>
```
#### 样式
``` html
<style>
.fox-scroll-pager {
  width: 100%;
  overflow-x: auto;
}

.fox-scroll-pager-warpper {
  display: flex;
  width: 100%;
}

.fox-scroll-indicator {
  position: absolute;
  bottom: 10px;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
  justify-content: center;
}

.fox-scroll-indicator-item {
  width: 8px;
  height: 8px;
  border-radius: 50%;
  margin: 0 4px;
}
</style>
```

## 注意事项

- **性能优化**：尽量避免在滚动过程中频繁调用 `calculateTotalPages`，可以根据实际需求使用防抖（debounce）或节流（throttle）来优化性能。
- **兼容性**：确保使用的是支持 `uni.createSelectorQuery` 的环境，如 `uni-app` 或 `Vue`。
